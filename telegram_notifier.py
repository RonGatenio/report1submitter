
# Read more about telegram API in: https://core.telegram.org/bots/api

from io import BytesIO
import requests


REQUEST_SEND_MESSAGE  = 'sendMessage'
REQUEST_SEND_PHOTO    = 'sendPhoto'
REQUEST_SEND_LOCATION = 'sendLocation'

BOT_API_URL = 'https://api.telegram.org/bot{bot_api_key}/{request}'


class TelegramNotifier:
    def __init__(self, bot_api_key, chat_id):
        self._bot_api_key = bot_api_key
        self._chat_id = chat_id

    def post(self, request, **parameters):
        url = BOT_API_URL.format(bot_api_key=self._bot_api_key, request=request)

        data = {}
        files = {}
        for k, v in parameters.items():
            if hasattr(v, 'read'):
                files[k] = v
            else:
                data[k] = v

        data['chat_id'] = self._chat_id

        response = requests.post(url, data=data, files=files)
        if response.status_code != 200:
            print('Fail request {} {} - {} ({})'.format(
                request,
                response.status_code, 
                response.reason,
                response.url))

        return response

    def send_message(self, text, **parameters):
        return self.post(REQUEST_SEND_MESSAGE, text=text, **parameters)

    def send_photo_by_url(self, url, caption=None, **parameters):
        return self.post(REQUEST_SEND_PHOTO, photo=url, caption=caption, **parameters)

    def send_photo_by_filename(self, filename, caption=None, **parameters):
        with open(filename, 'rb') as photo:
            return self.post(REQUEST_SEND_PHOTO, photo=photo, caption=caption, **parameters)

    def send_photo_by_bytes(self, photo_bytes, caption=None, **parameters):
        with BytesIO(photo_bytes) as photo:
            return self.post(REQUEST_SEND_PHOTO, photo=photo, caption=caption, **parameters)

    def send_location(self, longitude, latitude, **parameters):
        return self.post(REQUEST_SEND_LOCATION, longitude=longitude, latitude=latitude, **parameters)
