import os
from time import sleep
from urllib.parse import urljoin
import json
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from chrome_driver.chrome_browser import open_chrome_browser


BASE_URL            = 'https://one.prat.idf.il/'
LOGIN_URL           = urljoin(BASE_URL, 'login')
COOKIES_JSON_FILE   = 'cookies.json'
CAPTCHA_TIMEOUT_SEC = 1000


class Submitter:
    def __init__(self, telegram_notifier=None):
        self._telegram = telegram_notifier

    def register_user(self, outfile=COOKIES_JSON_FILE):
        """
        Let the user solve the I'm not a robot challenge and save the cookies
        """
        
        help_message = 'I need cookies!\nSolve the captcha, choose to stay signed in, and click enter'

        with open_chrome_browser(show_browser=True) as browser:
            browser.get(LOGIN_URL)
            browser.pop_alert(help_message)

            wait = WebDriverWait(browser, CAPTCHA_TIMEOUT_SEC)
            wait.until(EC.url_changes(LOGIN_URL))
            
            sleep(1)

            cookies = browser.get_cookies()

        if outfile:
            with open(outfile, 'w') as f:
                json.dump(cookies, f, indent=4)

        return cookies

    def _submit(self, cookies, show_browser=False):
        self._telegram and self._telegram.send_message('Started submission for 1 report')
        with open_chrome_browser(show_browser=show_browser) as browser:
            try:
                browser.get(BASE_URL)
                for c in cookies:
                    browser.add_cookie(c)
                browser.get(BASE_URL)

                sleep(10)

                # Close warning box if present
                try:
                    browser.click_element('//div[@class="warningBox"]/button')
                except:
                    pass

                elements = browser.find_elements_by_xpath('//button[contains(@class, "locationStatusBtn")]')
                assert len(elements) == 1
                elements[0].click()

                sleep(10)
                data = browser.get_screenshot_as_png()
                self._telegram and self._telegram.send_photo_by_bytes(data, caption='Submission successful')
            except:
                data = browser.get_screenshot_as_png()
                self._telegram and self._telegram.send_photo_by_bytes(data, caption='Failed to submit')

    def submit(self, cookies=None, show_browser=False):
        """
        Submit the "1 report" form.

        ``cookies`` are your session cookies for the form. Can be:
        - Array of cookies
        - Path to a json file with the cookies array
        - ``None`` - the default json file will be used if it exists.
          if not, ``register_user`` will be invoked

        ``show_browser`` - show browser if True, or not otherwise
        """

        # Get the cookies
        if not cookies:
            cookies = COOKIES_JSON_FILE

        if isinstance(cookies, str):
            if os.path.isfile(cookies):
                with open(cookies, 'r') as f:
                    cookies = json.load(f)
            else:
                cookies = self.register_user()

        # Submit the form
        self._submit(cookies, show_browser)


def main():
    Submitter().submit()


if __name__ == "__main__":
    main()
