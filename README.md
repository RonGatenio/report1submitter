# Report 1 Submitter
Submit 1 form with selenium

## How to run
Just run the script `main.py`.

### Download chromedriver.exe
For the first time, you should download the `chromedriver.exe`.  
Do this by passing `-d` to the script.

### Registration
You need to register at least once to the site.  
The script will prompt the registration process if needed.

After registration, a new `cookies.json` file will be created.  
As long as it is there, you won't need to register again.

If you just want to register, pass `-r` to the script.

## Telegram Notifications Support
- Create a telegram bot using [@botfather](https://t.me/botfather)
  - Note the bot's API key
- Add the bot to a telegram channel as an admin
  - Note the channel's API key
- Pass the following arguments to the script:  
  `-t BOT_API_KEY CHANNEL_NAME`

### Private Telegram Channels
Follow these steps to get the private channel's name:
- Use [telegram web](https://web.telegram.org) to enter the channel
- The URL should look like: `https://web.telegram.org/#/im?p=cXXXXXXXXXX_YYYYYYYYYYYYYYYYYYY`
- The channel name is `-100XXXXXXXXXX`
