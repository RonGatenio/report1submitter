import argparse
import submitter
from telegram_notifier import TelegramNotifier


def parse_arguments(args=None):
    parser = argparse.ArgumentParser()

    parser.add_argument('-c', '--cookies',
                        metavar='JSON_FILE',
                        default=submitter.COOKIES_JSON_FILE,
                        help='Json cookies file')

    parser.add_argument('-r', '--register',
                        action='store_true',
                        help='Only register the user')

    parser.add_argument('-t', '--telegram',
                        nargs=2,
                        metavar=('BOT_API_KEY', 'CHANNEL_NAME'),
                        default=None,
                        help='Telegram bot parameters')

    parser.add_argument('-d', '--download_chromedriver',
                        action='store_true',
                        default=False,
                        help='Download the chromedriver if needed')

    parser.add_argument('-v', '--show_browser',
                        action='store_true',
                        default=False,
                        help='Show browser while running')

    return parser.parse_args(args)


def main_cli(args=None):
    args = parse_arguments(args)

    if args.download_chromedriver:
        # Lazy import
        from download_chromedriver import download_chromedriver
        download_chromedriver()

    telegram = TelegramNotifier(*args.telegram) if args.telegram else None
    
    s = submitter.Submitter(telegram)
    
    if args.register:
        s.register_user(args.cookies)
    else:
        s.submit(args.cookies, args.show_browser)


if __name__ == "__main__":
    main_cli()
